/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';

import {
  StackNavigator
} from 'react-navigation';

import AuthScreen from './pages/AuthScreen';
import ProfilScreen from './pages/ProfilScreen';

const AppNavigator = StackNavigator ({
  Auth: {
    screen: AuthScreen
  },
  Profil: {
    screen: ProfilScreen
  }
});

export default class App extends Component{
  render() {
    return (
      <AppNavigator />
    );
  }
}
