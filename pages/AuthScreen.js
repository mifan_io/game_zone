import React, { Component } from 'react';
import { 
  View, 
  Text,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView,
  StyleSheet
} from 'react-native';

import { 
  KeyboardAwareScrollView 
} from 'react-native-keyboard-aware-scroll-view'

import PhoneInput from 'react-native-phone-input'

import {
  widthPercentageToDP as wp, heightPercentageToDP as hp
} from 'react-native-responsive-screen';

export class AuthScreen extends Component {
  static navigationOptions = {
    header: null
  }
  constructor() {
    super()
  }
  onPress = () => {
  } 
  render() {
    return (
      <KeyboardAwareScrollView
        resetScrollToCoords={{ x: 0, y: 0 }}
        contentContainerStyle={styles.container}
        scrollEnabled={false}
        >
        <View style={styles.container}>
          <View style={styles.headerAuth}>
            <View style={styles.headerAuthContent}>
              <Text style={styles.headerAuthContentTitle}>GAME ZONE</Text>
            </View>
          </View>
          <View style={styles.formAuth}>
            <Text style={styles.formAuthContentTitle}>PHONE NUMBER</Text>
            <View style={styles.mainFormNumber}>
              <PhoneInput ref='phone' initialCountry='id' keyboardType='numeric' maxLength={12} style={styles.mainFormPlaceHolder}></PhoneInput>
            </View>
            <Text style={styles.formAuthContentTitle}>PASSWORD</Text>
            <View style={styles.mainForm}>
              <TextInput secureTextEntry={true} placeholder='********' placeholderTextColor="#000" style={styles.mainFormPlaceHolder}></TextInput>
            </View>
            <View style={styles.formAuthContent}>
              <View>
                <TouchableOpacity onPress={this.onPress}>
                  <Text style={styles.formAuthContentTitle}>Forgot password ??</Text>
                </TouchableOpacity>
              </View>
              <View>
                <TouchableOpacity onPress={this.onPress}>
                  <Text style={styles.formAuthContentTitle}>Register new account</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View>
              <TouchableOpacity onPress={this.onPress} style={styles.mainButton}>
                <Text style={styles.mainButtonTitle}>LOGIN</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </KeyboardAwareScrollView>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#0C0D16'
  },
  headerAuth: {
    height: hp('45%')
  },
  headerAuthContent: {
    padding: hp('3%')
  },
  headerAuthContentTitle: {
    fontSize: 40,
    color: '#EB9E27',
    fontFamily: 'Questrial-Regular',
    letterSpacing: 2
  },
  formAuth: {
    height: hp('55%'),
    padding: hp('3%'),
    flexDirection: 'column',
    justifyContent: 'flex-end'
  },
  formAuthContent: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  formAuthContentTitle: {
    fontFamily: 'OpenSans-Regular',
    letterSpacing: 0.5,
    marginTop: hp('3hp'),
    color: '#E0DDDD'
  },
  mainFormNumber: {
    backgroundColor: '#2D3049',
    paddingTop: hp('2hp'),
    marginTop: hp('1hp'),
    paddingBottom: hp('2hp'),
    borderRadius: 5
  },
  mainForm: {
    backgroundColor: '#2D3049',
    paddingTop: hp('2hp'),
    marginTop: hp('1hp'),
    borderRadius: 5
  },
  mainFormPlaceHolder: {
    paddingLeft: hp('2hp'),
    color: '#E0DDDD'
  },
  mainButton: {
    alignItems: 'center',
    backgroundColor: '#EB9E27',
    padding: hp('3hp'),
    marginTop: hp('3hp'),
    marginBottom: hp('4hp'),
    borderRadius: 5
  },
  mainButtonTitle: {
    textTransform: 'uppercase',
    fontFamily: 'OpenSans-Regular',
    letterSpacing: 5,
    color: '#0C0D16'
  }
});

export default AuthScreen;